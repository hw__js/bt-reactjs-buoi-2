import React, { Component } from "react";

export default class InfroGlasses extends Component {
  render() {
    let { url, name, price, desc } = this.props.detail;

    return (
      <div>
        <img src={url} alt="" className="glassesOnModel position-absolute" />

        <div id="glassesInfo" className="vglasses__info position-relative">
          <h5>{name}</h5>
          <span className="bg-danger p-1 rounded">{price}$</span>
          <span className="text-success ml-1"> Stocking</span>
          <p className="mt-2">{desc}</p>
        </div>
      </div>
    );
  }
}
