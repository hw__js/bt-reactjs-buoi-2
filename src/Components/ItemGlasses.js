import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="item col-2 p-2">
        <img
          src={url}
          alt=""
          onClick={() => {
            this.props.handleClick(this.props.data);
          }}
        />
      </div>
    );
  }
}
