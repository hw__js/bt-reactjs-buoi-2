import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import InfroGlasses from "./InfroGlasses";
import ItemGlasses from "./ItemGlasses";
import ModelImage from "./ModelImage";

export default class ShowGlasses extends Component {
  state = {
    glassesArr: dataGlasses,
    detail: dataGlasses[0],
  };

  renderListGlasses = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
          handleClick={this.handleChangeDetailGlasses}
          data={item}
          key={index}
        />
      );
    });
  };

  handleChangeDetailGlasses = (glasses) => {
    this.setState({
      detail: glasses,
    });
  };

  render() {
    return (
      <div id="abc">
        <div className="content">
          <h3>TRY GLASSES APP ONLINE</h3>

          {/* Model  */}
          <div className="container model">
            <div className="row mt-5">
              <div className="img1 col-6 ">
                <ModelImage />

                <InfroGlasses detail={this.state.detail} />
              </div>

              <div className="img col-6">
                <ModelImage />
              </div>
            </div>
            {/* Model  */}

            {/* show Glasses List  */}
            <div className="container">
              <div className="glassesList row py-5">
                {this.renderListGlasses()}
              </div>
            </div>
            {/* show Glasses List  */}
          </div>
        </div>
      </div>
    );
  }
}
