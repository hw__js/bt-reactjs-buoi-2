import React, { Component } from "react";

export default class ModelImage extends Component {
  render() {
    return (
      <img
        src="./glassesImage/model.jpg"
        alt=""
        style={{ width: "40%", height: "fit-content", position: "absolute" }}
      />
    );
  }
}
